provider "google" {
  version     = "3.16.0"
  credentials = file("terraform-key.json")

  project = var.project_id
  region  = var.region
}

provider "google-beta" {
  version     = "3.29.0"
  credentials = file("terraform-key.json")
  project     = var.project_id
  region      = var.region
}