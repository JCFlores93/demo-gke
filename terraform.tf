terraform {
    backend "gcs" {
        credentials = "terraform-key.json"
        bucket = "terraform-demo-flores-carrasco"
        prefix = "terraform/state"
    }
}